# ****************************************************************************#
#                                                                             #
#                                                         :::      ::::::::   #
#    CsvReader.py                                       :+:      :+:    :+:   #
#                                                     +:+ +:+         +:+     #
#    By: jojomoon <jojomoon@student.42lyon.fr>      +#+  +:+       +#+        #
#                                                 +#+#+#+#+#+   +#+           #
#    Created: 2020/05/28 15:38:02 by jojomoon          #+#    #+#             #
#    Updated: 2020/05/28 15:38:05 by jojomoon         ###   ########lyon.fr   #
#                                                                             #
# ****************************************************************************#

import os
import csv
import time
import getpass


logg = open("./machine.log", "w+")


class FileError(Exception):
    def __init__(self, message):
        self.message = message


def log(func):
    def wrap(*args, **kwargs):
        username = getpass.getuser()
        logg.write('(LogFunction)Running:\n'
                   .format(username, "log", "n.nan", "na"))
        time1 = time.time()
        ret = func(*args, *kwargs)
        time2 = time.time()

        timer = (time2-time1) * 1000
        measure = "ms"
        if timer >= 1000:
            measure = "s"
            timer = timer / 1000
        logg.write('\t({})Running: {:13} [ exec-time = {:5.3f} {:2} ]'
                   .format(username, func.__name__, timer, measure))
        logg.write("\t| " + str(args) + str(kwargs) + " |\n")
        return ret
    return wrap


class CsvReader:

    def __init__(self, filename=None, header=False, sep=",",
                 skip_top=0, skip_bottom=0):
        self.created = False
        self.tab = list() if os.path.exists(filename) else None
        if not os.path.exists(filename):
            raise FileNotFoundError(f"file '{filename}' doesn't exist")
        self.file = open(filename, newline='')
        reader = csv.reader(self.file, delimiter=sep)
        if header:
            self.features = reader.__next__()
            skip_top -= 1 if skip_top > 0 else 0
        else:
            self.features = None
        self.data = [row for row in reader]
        if self.data == []:
            raise FileError("file is empty")
        self.nbFeatures = len(self.features) if header else len(self.data[0])
        if not 0 <= skip_top + skip_bottom <= len(self.data):
            raise ValueError("skip combined values are bigger than dataset")
        if skip_top < 0 or skip_bottom < 0:
            raise ValueError("skip can not be negative")
        self.skip_top = skip_top
        self.skip_bottom = skip_bottom
        if all(len(row) == len(self.nbFeatures) for row in self.tab):
            self.created = True

    @log
    def __enter__(self):
        if self.created:
            return self
        return None

    @log
    def __exit__(self, type, value, traceback):
        self.file.close()

    @log
    def getdata(self):
        print(len(self.data))
        return self.data[None if self.skip_top == 0 else self.skip_top:
                         len(self.data) - self.skip_bottom]

    @log
    def getheader(self):
        return self.features


def main():
    try:
        with CsvReader('good.csv', header=False,
                       skip_top=100, skip_bottom=800) as file:
            if file is None:
                print("File is corrupted")
                return
            data = file.getdata()
            print(len(data))
            print(data[0])
            print(data[-1])
            header = file.getheader()
            print(header)
    except ValueError as e:
        print("ValueError: " + str(e))
    except FileNotFoundError as e:
        print("FileNotFoundError: " + str(e))
    except FileError as e:
        print("FileError: " + e.message)


if __name__ == "__main__":
    main()
    logg.close()
