# ****************************************************************************#
#                                                                             #
#                                                         :::      ::::::::   #
#    ft_map.py                                          :+:      :+:    :+:   #
#                                                     +:+ +:+         +:+     #
#    By: jojomoon <jojomoon@student.42lyon.fr>      +#+  +:+       +#+        #
#                                                 +#+#+#+#+#+   +#+           #
#    Created: 2020/05/28 10:15:36 by jojomoon          #+#    #+#             #
#    Updated: 2020/05/28 10:15:50 by jojomoon         ###   ########lyon.fr   #
#                                                                             #
# ****************************************************************************#

# https://docs.python.org/3.7/library/functions.html#map
def ft_map(function_to_apply, *list_of_inputs):
    common_length = len(list_of_inputs[0])
    for inpt in list_of_inputs[1:]:
        if len(inpt) < common_length:
            common_length = len(inpt)
    return [
        function_to_apply(*(elem[i] for elem in list_of_inputs))
        for i in range(common_length)
    ]
