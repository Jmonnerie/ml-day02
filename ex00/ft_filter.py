# ****************************************************************************#
#                                                                             #
#                                                         :::      ::::::::   #
#    ft_filter.py                                       :+:      :+:    :+:   #
#                                                     +:+ +:+         +:+     #
#    By: jojomoon <jojomoon@student.42lyon.fr>      +#+  +:+       +#+        #
#                                                 +#+#+#+#+#+   +#+           #
#    Created: 2020/05/28 10:16:04 by jojomoon          #+#    #+#             #
#    Updated: 2020/05/28 10:16:06 by jojomoon         ###   ########lyon.fr   #
#                                                                             #
# ****************************************************************************#

# https://docs.python.org/3/library/functions.html#filter
def ft_filter(function_to_apply, list_of_inputs):
    if function_to_apply is not None:
        return (
            item
            for item in list_of_inputs
            if function_to_apply(item)
        )
    return (
        item
        for item in list_of_inputs
        if item
    )
