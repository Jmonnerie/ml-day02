# ****************************************************************************#
#                                                                             #
#                                                         :::      ::::::::   #
#    ft_reduce.py                                       :+:      :+:    :+:   #
#                                                     +:+ +:+         +:+     #
#    By: jojomoon <jojomoon@student.42lyon.fr>      +#+  +:+       +#+        #
#                                                 +#+#+#+#+#+   +#+           #
#    Created: 2020/05/28 10:16:13 by jojomoon          #+#    #+#             #
#    Updated: 2020/05/28 10:16:17 by jojomoon         ###   ########lyon.fr   #
#                                                                             #
# ****************************************************************************#

def ft_reduce(function_to_apply, list_of_inputs):
    tmp = None
    for elem in list_of_inputs:
        tmp = el if tmp is None else function_to_apply(tmp, elem)
    return tmp