# ****************************************************************************#
#                                                                             #
#                                                         :::      ::::::::   #
#    main.py                                            :+:      :+:    :+:   #
#                                                     +:+ +:+         +:+     #
#    By: jojomoon <jojomoon@student.42lyon.fr>      +#+  +:+       +#+        #
#                                                 +#+#+#+#+#+   +#+           #
#    Created: 2020/05/28 12:01:57 by jojomoon          #+#    #+#             #
#    Updated: 2020/05/28 12:38:32 by jojomoon         ###   ########lyon.fr   #
#                                                                             #
# ****************************************************************************#

def what_are_the_vars(*args, **kwargs):
    obj = ObjectC()
    for key, value in kwargs.items():
        setattr(obj, key, value)
    for i, val in enumerate(args):
        key = f"var_{i}"
        if hasattr(obj, key):
            return None
        setattr(obj, key, val)
    return obj


class ObjectC(object):
    def __init__(self):
        pass


def doom_printer(obj):
    if obj is None:
        print("ERROR")
        print("end")
        return
    for attr in dir(obj):
        if attr[0] != '_':
            value = getattr(obj, attr)
            print("{}: {}".format(attr, value))
    print("end")


if __name__ == "__main__":
    print("======================== 7")
    obj = what_are_the_vars(7)
    doom_printer(obj)
    print("======================== 'ft_lol', 'Hi'")
    obj = what_are_the_vars("ft_lol", "Hi")
    doom_printer(obj)
    print("======================== ''")
    obj = what_are_the_vars()
    doom_printer(obj)
    print("======================== 12, 'Yes', [0, 0, 0], a=10, hello='world'")
    obj = what_are_the_vars(12, "Yes", [0, 0, 0], a=10, hello="world")
    doom_printer(obj)
    print("======================== 42, a=10, var_0='world'")
    obj = what_are_the_vars(42, a=10, var_0="world")
    doom_printer(obj)
